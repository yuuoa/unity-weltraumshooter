﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scoring : MonoBehaviour
{
    public static int scoreValue;

    Text scoreNow;

    void Awake()
    {
        scoreNow = GetComponent<Text>();
        scoreValue = 0;
    }

    void Update()
    {
        scoreNow.text = "Score : " + scoreValue;
    }
}
