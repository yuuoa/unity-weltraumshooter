﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class menuPad : MonoBehaviour
{
    [SerializeField] public GameObject pad;
    
    public void menuButtonDown()
    {
        pad.SetActive(true);
    }

    public void closeButtonDown()
    {
        pad.SetActive(false);
    }
}
