﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemyplane : MonoBehaviour
{
    [SerializeField] public AudioSource laser;
    [SerializeField] public AudioSource destroyed;
    public Animator animator;
    public Transform player;
    private Rigidbody2D rb;
    private Vector2 movement;
    public Transform firePoint;
    public GameObject bulletPrefab;

    private float nextFireTime;
    public float fireRate = 1f;
    public float bulletForce = 10f;
    public float moveSpeed = 1f;
    public int scoreValue = 100;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        player = GameObject.Find("player").GetComponent<Transform>();
    }

    void Update()
    {
        Vector3 direction = player.position - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        rb.rotation = angle-90;
        direction.Normalize();
        movement = direction;
        Bullet();

    }

    void Bullet()
    {
        if (nextFireTime <Time.time)
        {
            laser.Play();
            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(firePoint.up * bulletForce, ForceMode2D.Impulse);
            nextFireTime = Time.time + fireRate;
        }
    }

    private void FixedUpdate()
    {
        moveCharacter(movement);
    }

    void moveCharacter (Vector2 direction) {
        {
            rb.MovePosition((Vector2)transform.position + (direction * moveSpeed * Time.deltaTime));
        }
    }
    
    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.transform.tag == "Player bullet")
        {
            destroyed.Play();
            animator.SetBool("destroyed", true);
            StartCoroutine(planeDestroyed());
        }
    }

    IEnumerator planeDestroyed ()
    {
        scoring.scoreValue += scoreValue;
        yield return new WaitForSeconds (1f);
        Destroy(gameObject);
    }
}
