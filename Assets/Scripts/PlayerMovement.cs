﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public Joystick joystick;
    float z;
    float _z;
    float __z;
    
    public bool shootButton;
    
    private float nextFireTime;
    public float fireRate = 1f;
    public float bulletForce = 20f;

    public float moveSpeed = 5f;

    [SerializeField] private AudioSource laser;
    [SerializeField] private AudioSource destroyed;
    public Transform firePoint;
    public GameObject bulletPrefab;
    public Rigidbody2D rb;
    public Camera cam;
    public GameObject GameOver;
    public Animator animator;
    Text scoringUI;

    Vector2 movement;

    Enemyplane bullet;

    void Start()
    {
        bullet = GameObject.Find("enemy").GetComponent<Enemyplane>();
        scoringUI = GameObject.Find("scoring").GetComponent<Text>();
    }

    void Update()
    {
        moving();
        shooting();
    }

    void moving()
    {
        movement.x = joystick.Horizontal;
        movement.y = joystick.Vertical;

        float hAxis = joystick.Horizontal;
        float vAxis = joystick.Vertical;
        float z = Mathf.Atan2(-hAxis, vAxis) * Mathf.Rad2Deg;
        if (z != 0)
        { 
            _z = z; 
            __z = z; 
        }
        else
        {
            _z=__z;
        }
    }

    void FixedUpdate()
    {
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
        rb.rotation = _z;
    }

    void Bullet()
    {
        if (nextFireTime <Time.time)
        {   
            laser.Play();
            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(firePoint.up * bulletForce, ForceMode2D.Impulse);
            nextFireTime = Time.time + fireRate;
        }
    }

    void shooting()
    {
        if(shootButton == true)
        {
            Bullet();
        }
    }

    public void shootButtonOn()
    {
        shootButton = true;
    }
    
    public void shootButtonOff()
    {
        shootButton = false;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.transform.tag == "Enemy bullet" || other.transform.tag == "enemy")
        {
            destroyed.Play();
            animator.SetBool("destroyed", true);
            StartCoroutine(planeDestroyed());
        }
    }

    IEnumerator planeDestroyed ()
    {
        yield return new WaitForSeconds (1f);
        Destroy(gameObject);
        GameOver.SetActive(true);
    }
}